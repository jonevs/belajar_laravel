<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome()
    {
        return view('welcome2');
    }

    public function welcomeName(Request $request)
    {
        // dd($request->all());
        $firstname = $request['firstName'];
        $lastname = $request['lastName'];
        return view('welcome2', ['firstname' => $firstname], ['lastname' => $lastname]);
    }
}
