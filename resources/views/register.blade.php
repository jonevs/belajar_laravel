<!DOCTYPE html>
<html>
    <head>
        <title>Halaman Form</title>
    </head>

    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="firstName"><br><br>

            <label>Last name:</label><br><br>
            <input type="text" name="lastName"><br><br>

            <label>Gender:</label><br><br>
            <input type="radio" name="gender"> Male<br> 
            <input type="radio" name="gender"> Female<br>
            <input type="radio" name="gender"> Other<br><br>

            <label>Nationality:</label><br><br>
            <select>
                <option value="indo">Indonesian</option>
            </select><br><br>

            <label>Language Spoken:</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br><br>

            <label>Bio:</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>        
            
            <input type="submit" value="Sign Up"> 
        </form>
    </body>
</html>